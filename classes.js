//Prepare the canvas and context
const WIDTH=1300;
const HEIGHT=550;
var canvas=document.getElementById("canvas");
canvas.setAttribute("width",WIDTH);
canvas.setAttribute("height",HEIGHT);
var context = canvas.getContext("2d");

class Tanya {
    constructor(){
        this.score=0;
        //constantes
                this.step=5;//it must be devisible by initV
                this.sizeCoef=1.1;//the coefficient of size the higher the big the image
                this.width=this.sizeCoef*45;
                this.height=this.sizeCoef*63;
        //coordinates and mouvement
                this.Y=HEIGHT/2;
                this.X=WIDTH/4;
                this.Vx=0;//how much x-quantity of mouvement is left
                this.Vy=35;//how much y-quantity of mouvement is left
        
        this.newFrame=function(){
            let step_1=this.step * Math.sin(20);
            let step_2=this.step * Math.cos(20);
            if(this.Vx>0){
                this.X +=step_1;
                this.Vx-=this.step;
            }else if(this.Vx<0){
                this.X -=step_1;
                this.Vx+=this.step;
            }
            if(this.Vy>0){//ATT_HI
                if(this.Vy>35)      this.Y +=2*step_2;
                else                this.Y +=step_2;
            }else{
                this.Y -=step_2;
            }
            this.image = new Image();
            if(this.Vy<0){
                this.image.src = "tanya_up.png";
            }else{
                this.image.src = "tanya.png";
            }        
            context.drawImage(this.image,this.X,this.Y,this.width,this.height);
            this.Vy=35;//restore the initial value of step
        }
    }
}
class Obstacle{
    constructor(x,y,image_src="v2.png"){
        //constantes
                this.step=6;
                this.sizeCoef=0.4;//the coefficient-size of the obstacles the higher the big the image
                this.sizeCoefD=0.4;//the coefficient-size of the detonator the higher the big the image
                this.height =this.sizeCoef*102;
                this.width  =this.sizeCoef*626;
                this.heightD=this.sizeCoefD*65;
                this.widthD =this.sizeCoefD*168;
        //coordinates and mouvement
                this.X=x;
                this.Y=y;
                this.detonated=false;
                this.Xd=0;
                this.Yd=0;
        this.NewFrame=function(){
            if(this.detonated){
                this.Y+=20;
            }else{
                this.X-=this.step;
                if(image_src!="Bullet.png"){
                    let imageD = new Image();
                    imageD.src = "detonator.png";
                    this.Xd=this.X+this.width/2-this.widthD/2;
                    this.Yd=this.Y-this.heightD+9;
                    //context.drawImage(imageD,0,0,this.widthD,this.heightD);
                    context.drawImage(imageD,this.Xd,this.Yd,this.widthD,this.heightD);
                }
            }                
            let image = new Image();
            image.src = image_src;
            context.drawImage(image,this.X,this.Y,this.width,this.height);
            if(this.step==12) this.step=6;//restore the initial value of step
            if(this.step==36) this.step=36;//restore the initial value of step
        }
    }
}