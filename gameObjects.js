var tanya=new Tanya(); 
var Obstacles=[];//every obstacle in the caneva is stored here
var Bullets_sign=[];//to show later a sign for every bullet
var keys=[];//resolve the problem of multiple keys pressed at the same time
let DIFFICULTY=200;//the lower the number the higher the difficulty
const TIME_SIGNALES=45;//for how much frames the signales will be shown
let image = new Image();//Do not ever touch this ligne
image.src = "explosion.png";//Do not ever touch this ligne

let nb_clicks=0;
var tutorial_frames=[];
for(let i=1;i<6;i++){//many hours for this,serve to load the images before being used
    tutorial_frames[i-1]=new Image();
    tutorial_frames[i-1].src="./Frames/"+i+".png";
}

window.addEventListener('click',function(){//here the game start
    nb_clicks++;
    let imagee=tutorial_frames[nb_clicks-1];
    switch (nb_clicks){
        case 1:case 2:case 3:case 4:case 5:
            context.drawImage(imagee,0,0,WIDTH,HEIGHT);
            break;
        case 6: 
            document.getElementById("sound").setAttribute("src","Victory_Or_Death.mp3");
            updateGameArea();
            break;
    }
})

function updateGameArea() {
    let statue=state();//the state of the game
    if(statue==0){
        draw_caneva();//draw the background image
        tanya.newFrame();//drawing tanya is the responsability of tanya's class
        draw_score(tanya.score);
        update_obstacles();
        generate_obstacles(DIFFICULTY);
        generate_bullet(2*DIFFICULTY,TIME_SIGNALES);
        update_draw_signales();
        draw_obstacles();
       
        for(let i=0;i<keys.length;i++){//update the properties of the objects based on the keys pressed
            update_objects(keys[i]);
        }
        detonator();
        window.requestAnimationFrame(updateGameArea);//regenerate a new frame
    }else{//somthing has gone wrong
        document.getElementById("sound").setAttribute("src","");
        let first =false;
        let second =false;
        switch (statue){
            case 1://crached
                first=crash(image);
                break;
            case 2://run away
                alert("A soldier never runs from the battlefield");
                second=true;
                break;
            case 3://no oxygen
                alert("No enought oxygen in high altitudes");
                second=true;
                break;
            case 4://low altitude
                alert("Flying at low altitude is dangerous");
                second=true;
                break;
        }
        /*
        Does not work require a more in deapth look at the languange
        let choice = window.confirm("Replay ?");
        if(choice){
            location.reload();
        }*/
    }
};
function update_obstacles(){
    for(let i=0;i<Obstacles.length;i++){
        if(Obstacles[i].X<-Obstacles[i].width){//remove old ones 
            Obstacles.splice(i,1);
        }else{//uddate the state of existing ones
            Obstacles[i].Vx+=2;
        }
    }
};
function draw_obstacles(){
    for(let i=0;i<Obstacles.length;i++){//draw the obstacles
        Obstacles[i].NewFrame();
    }
};
function update_draw_signales(){
    for(let i=0;i<Bullets_sign.length;i++){//draw a sign saying that a bullet is coming
        let element=Bullets_sign[i];
        if(element.time==0){
            Bullets_sign.splice(i,1);
            i--;
        }else{
            element.time--;
            let image = new Image();
            image.src = "danger.png";
            context.drawImage(image,WIDTH-60,element.y,50,50);
        }
    }
};
function draw_caneva(img="sky.jpg"){
    context.clearRect(0,0,WIDTH,HEIGHT);//clear the caneva for the new frame
    let image = new Image();
    image.src = img;
    context.drawImage(image,0,0,WIDTH,HEIGHT);
    if(tanya.score==26){
        context.font = "25px Trebuchet-MS";
        context.fillStyle="#dc143c";
        context.fillText("The end is near...",10,60);
    }
};
window.addEventListener('keydown', function (e) {
    for(let i=0;i<keys.length;i++){
        if(keys[i]==e.keyCode)
            return;
    }
    keys.push(e.keyCode);
});
window.addEventListener('keyup', function (e) {
    for(let i=0;i<keys.length;i++){
        if(keys[i]==e.keyCode){
            keys.splice(i,1);
            return;
        }   
    }
});
function update_objects (keyCode){
    let initV=35;//if changed you need to change ATT_HI
    switch (keyCode){
        case 39 ://right
            if(tanya.X<0.5*WIDTH){
                tanya.Vx=initV;
            }else{
                Obstacles.forEach(function(element){
                    element.step*=2;
                });
            }break;
        case 37 ://left
            tanya.Vx=(-initV);
            break;
        case 38 ://top
            //if(tanya.Vy<=0)     
                tanya.Vy=-initV;
            break;
        case 40 ://down
            tanya.Vy=2*initV;
    }
};
function generate_obstacles(frec=20){
    if(Math.floor(Math.random() * frec) == 1){
        Obstacles.push(new Obstacle(WIDTH,Math.floor((Math.random() * HEIGHT) + 1)));
    }
};
function generate_bullet(frec=20,timee){
    if(Math.floor(Math.random() * (frec+9)) == 1){
        let new_bullet=new Obstacle(WIDTH+7000,Math.floor((Math.random() * HEIGHT) + 1),"Bullet.png");
        new_bullet.step*=new_bullet.step*3;
        new_bullet.sizeCoef=0.8;
        new_bullet.height =new_bullet.sizeCoef*37;
        new_bullet.width  =new_bullet.sizeCoef*218;
        Obstacles.push(new_bullet);
        Bullets_sign.push({y:new_bullet.Y,time:timee});
    }
};
function inRange(a,b,x,marge=0){//a+a*m<x<b-b*m
    if(  ( (a+a*marge)<x ) && ( x<(b-b*marge) )  )  return true;
    return false;
};
function state(){
    //0 :no problem
    //1 :crached
    //2 :run away
    //3 :no oxygen
    //4 :low altitude
    let marge=0.03;
    for(let i=0;i<Obstacles.length;i++){
        let element=Obstacles[i];
        let op1=inRange(element.X,element.X+element.width,tanya.X,marge);
        let op2=inRange(element.X,element.X+element.width,tanya.X+tanya.width,marge);
        let op3=inRange(element.Y,element.Y+element.height,tanya.Y,marge);
        let op4=inRange(element.Y,element.Y+element.height,tanya.Y+tanya.height,marge);
        if( (op1 || op2) && (op3 || op4) )  return 1;
    }
    if(tanya.X<0)               return 2;
    if(tanya.Y<-tanya.height)   return 3;
    if(tanya.Y>HEIGHT)          return 4;
    return 0;
};
function crash(image){
    context.drawImage(image,tanya.X-300,tanya.Y-300,600,600);
    document.getElementById("sound").setAttribute("src","explosion.mp3");
    context.beginPath();
    context.lineWidth = "5";
    context.fillStyle = "#FF0000";
    context.fillRect(WIDTH-170,8,170,35);
    context.stroke();
    draw_score(tanya.score,"#0050ff","Your score: ");
    console.log("one");
    return true;
};
function detonator(){
    for(let i=0;i<Obstacles.length;i++){
        let element=Obstacles[i];
        let op1=inRange(element.Xd,element.Xd+element.widthD,tanya.X,0);
        let op2=inRange(element.Xd,element.Xd+element.widthD,tanya.X+tanya.width,0);
        let op3=inRange(element.Yd,element.Yd+element.heightD,tanya.Y,0);
        let op4=inRange(element.Yd,element.Yd+element.heightD,tanya.Y+tanya.height,0);
        if( (op1 || op2) && (op3 || op4) && (!element.detonated)){
            tanya.score++;
            element.detonated=true;
            if(DIFFICULTY>10){//increase the difficulty of the game
                DIFFICULTY-=7;
                console.log(DIFFICULTY);
            }
        }
        if(element.Y>HEIGHT) 
            Obstacles.splice(i,1);
    }
};
function draw_score(score,color="black",phrase="Current score: "){
    context.font = "20px Trebuchet-MS";
    context.fillStyle=color;
    context.fillText(phrase+score,1150,30);
};